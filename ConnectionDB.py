'''Arquivo de conexao com a banco de dados dataAnalise'''


import mysql.connector
from mysql.connector import errorcode

class Conexao():
    def __init__(self, host, user, password, database):
        config = {
            'host': host,
            'user': user,
            'password': password,
            'database': database,
        }

        try:
            self.conn = mysql.connector.connect(**config)
            # return "Connection established"
            self.cursor = self.conn.cursor()
            self.cursor.execute("CREATE TABLE IF NOT EXISTS compare (id int PRIMARY KEY NOT NULL AUTO_INCREMENT, "
                                "valor_cartao DECIMAL(10,2), valo_banco DECIMAL(10,2), comp DECIMAL(10,2));")
            print("Finished creating table.")
            #return self.cursor.cursor()
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                return "Something is wrong with the user name or password"
            elif err.errno == errorcode.ER_BAD_DB_ERROR:
                return "Database does not exist"
            else:
                return err
    def __exit__(self):
        self.cursor.close()

    def read(self, sql):
        self.cursor.execute(sql)
        self.rows = self.cursor.fetchall()
        for row in self.rows:
            print("%s %s %s %s" % (str(row[0]), str(row[1]), str(row[2]), str(row[3])))


    def inserir(self, valorCartao, valorBanco ):
        self.cursor.execute("INSERT INTO compare (valor_cartao, valor_banco, comp)"
                            " VALUES (%s, %s, %s);", (str(valorCartao), str(valorBanco), str((valorCartao - valorBanco))))
        self.conn.commit()


    def delete(self, tabela, id):
        self.cursor.execute("DELETE FROM %s WHERE id=%s;" % (str(tabela), str(id)))
        print("Deleted", self.cursor.rowcount, "row(s) of data.")
        self.conn.commit()

    def atualizar(self, tabela, campo, novoValor, id):
        self.cursor.execute("UPDATE %s SET %s = %s WHERE id = %s;" % (str(tabela), str(campo), str(novoValor), str(id)))
        print("Updated", self.cursor.rowcount, "row(s) of data.")
        self.conn.commit()

    def fecharConexao(self):
        self.cursor.close()

'''teste = Conexao('localhost', 'root', 'jordanw', 'dataAnalise')

#teste.delete('compare', 1)
teste.atualizar('compare', 'valor_banco', '740', 2)
sql = 'SELECT * FROM compare'
teste.read(sql)
'''